# (C) 2021 Thomas Pitschel. 
# MIT License.

class InsertionSorterItem:
    def __init__(self, k, data):
        self.k = k
        self.data = data
        
    def get_sort_key(self):
        return self.k
        
    def __str__(self):
        return "{}-{}".format(self.k, self.data.__str__())
        

class InsertionSorter:

    def __init__(self, max_size = -1):
        self.ls = []
        self.max_size = max_size
        
    def bisection_search(self, new_item):
        """Returns where to insert the new item."""
        search_k = new_item.get_sort_key()
        
        L = 0
        R = len(self.ls)    # point one right of last elem
        while L < R:
            if R-L == 1:
                if search_k <= self.ls[L].k:
                    return L
                else:
                    return R
            mid = (L+R) // 2
            if self.ls[mid].k < search_k:
                # new_item into [mid,R]
                L = mid
                continue
            else:
                # new item into [L,mid]
                R = mid
                continue
        return L
        
    def insert(self, new_item):
        inspos = self.bisection_search(new_item)
        self.ls.insert(inspos, new_item)
        if (self.max_size >= 0) and (len(self.ls) > self.max_size):
            del self.ls[self.max_size:]
        
    def print(self):
        i=0
        for el in self.ls:
            print(i, el)
            i += 1


if __name__ == "__main__":

    if 1:
        import random as rn
        ins1 = InsertionSorter(max_size=10)
        for k in range(20):
            new_item = InsertionSorterItem(rn.randint(0, 20), None)
            print("Inserting ", new_item.k)
            ins1.insert(new_item)
        ins1.print()
        