# An extension of the Graph class in module "dijkstra". 
# (C) 2021 Thomas Pitschel.

from dijkstra import Graph

class GraphExt:
    """
    This implements some extensions to the
    original Graph class from 
    https://github.com/ahojukka5/Dijkstra/blob/master/dijkstra/graph.py
    
    The __edge_list enables to address edges 
    using a "linear" integer index.
    """
    
    def __init__(self, adjacency_list=dict(), edge_weights=dict()):
        self.G = Graph(adjacency_list, edge_weights)
        self.__edge_list = []           # <- New
        self.__edge_indices_map = dict()# <- New
        i=0
        for edge in edge_weights:
            self.__edge_list.append(edge)
            self.__edge_indices_map[edge[0],edge[1]] = i
            i += 1
        
    def add_edge(self, u, v, w):
        """Returns edge index."""
        if (u not in self.G._Graph__adjacency_list) or \
            (v not in self.G._Graph__adjacency_list[u]):
            # This is a new edge, so add it to the list
            self.__edge_indices_map[u,v] = len(self.__edge_list)
            self.__edge_list.append((u,v))
        self.G.add_edge(u,v,w)
        return len(self.__edge_list)-1
        
    def get_edge_weight(self, u, v):
        return self.G.get_edge_weight(u, v)
        
    # New:
    def set_edge_weight(self, u, v, w):
        """Requires that the edge (u,v) already exists in the graph."""
        self.G._Graph__edge_weights[u, v] = w
        
    # New:
    def get_edge(self, ind):
        return self.__edge_list[ind]
        
    # New:
    def get_index_for_edge(self, u, v): 
        """Requires that the edge specified in fact is in the graph."""
        return self.__edge_indices_map[u,v]
        
    # New:
    def get_edges(self):
        return self.__edge_list
        
    def get_adjacent_nodes(self, u):
        return self.G.get_adjacent_nodes(u)

    def get_number_of_nodes(self):
        return self.G.get_number_of_nodes()

    # New:
    def get_number_of_edges(self):
        return len(self.__edge_list)

    def get_nodes(self):
        return self.G.get_nodes()

    def __str__(self):
        return self.G.__str__()
        
    # New:
    def node_path_to_edge_list(self, node_path):
        """Expects in node_path a list of nodes. Returns a list
            of edge indices."""
        res = []
        for k in range(1, len(node_path)):
            res.append(self.get_index_for_edge(node_path[k-1], node_path[k]))
        return res
