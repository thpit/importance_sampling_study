import numpy as np

from GraphExt import GraphExt
from ShortestPathIS import ShortestPathIS

if __name__ == "__main__":
    S,A,B,T = nodes = list("SABT")
    
    G = GraphExt()
    G.add_edge(S, A, 1)
    G.add_edge(S, B, 1)
    G.add_edge(A, B, 1)
    G.add_edge(A, T, 1)
    G.add_edge(B, T, 1)
    
    # add reverse edges in same order:
    nEdges = G.get_number_of_edges()
    for ind in range(nEdges):
        edge = G.get_edge(ind)
        G.add_edge(edge[1], edge[0], 1)

    print("Testing crude monte carlo estimate")
    
    #rng = default_rng()
    #rn.set_state(10)
    
    spis1 = ShortestPathIS(G, S, T)
    vec_u = 0.1085 * np.array([1.0, 1.0, 1.0, 1.0, 1.0])
    gamma = 3.7
    N = 100000
    P_est = spis1.crude_monte_carlo_estimate(vec_u, gamma, N)
    print("P_est_CMC: ", P_est)
    
