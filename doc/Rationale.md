## Importance sampling: overview
    
Given: a network of communication/transport channels, represented as a 
graph G, and a start and end node (S and T) from and to which data or goods
are to be transported. Each channel may fail, or in fact has
a gradual failure score (=cost) associated with it. The cost of transportation
along a path is the sum of the scores along that path.
But the best, smallest-cost, path can be chosen.

* Task 1 (following the introductory example in [1]): 
  Compute the expected cost of the transport from S to T.
* Task 2: see below.


### Task 1

Task 1 is formally written as seeking

    \int d(w) p(w) \d w,
    
where w is a vector consisting of all edge weights, and d(w) the 
shortest path cost for a given weight set w.
    
In realistic parametrizations, the probability of large
costs at a single edge are low. As effect, large path costs
are seldom and so in the integral two counteracting influences
determine the integrand value. Determining the integral value
by sampling then entails large variance.

Enter the variance reduction method:

    A = \int d(w) p(w) \d w
        = \int d(w)/h(w)  *  p(w)*h(w) \d w
        
To showcase, use a concrete example for p(w).

Let the edge weights be exponentially distributed,
say respectively with parameter \mu_i. I.e., the single edge weight W_i
is distributed with pdf \exp(- w_i / \mu_i) / \mu_i. As above the shortest
path is defined as d(\vec W). The expected cost is

    \int d(w) p(w) \d w.
    
It is p(w) = \prod_i \exp(-w_i / \mu_i) / \mu_i if all edge weights are stoch. indep., 
where the product is running over all edges. The largest shortest-path weight 
arises if many weights are large, which is a rare event. Thus employ ImpSampling
via a "shift" of p(.), say q(w) = p(w) * h(w). Let q(w) be chosen as another
multivariate exponential distribution, say with parameter vector \vec \nu.
Then 

    h(w) = q(w)/p(w) = \prod_i \frac{\mu_i}{\nu_i} \exp( - w_i / (1/\nu_i - 1/\mu_i) ).
    
If \nu_i is large, the corresp. expectation is large. Then 
the exponent in h(x) is positive at positive w_i and so is large
for large w_i. Rewriting the integral A yields

    A = \int d(w)/h(w)  * q(w) \d w
    
The empirical expectation of f(w) = d(w)/h(w) with respect to density q(w), e.g. 
via MCMC sampling of f using q, thus yields an approximation to the desired value A
with smaller variance. 


### Task 2

Task 2: Compute the probability that d(W) exceeds a threshold \gamma.

Approach: same as before with d(w) replaced by 1_{d(w) \leq \gamma}.


### Ref

## References 

[1] Reuven Y. Rubinstein, Dirk P. Kroese: "The Cross-Entropy method". Springer, 2004.
