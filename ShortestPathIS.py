# (C) 2021 Thomas Pitschel.
# (see references also)

import numpy as np
#from numpy.random import default_rng
import random as rn

from dijkstra import DijkstraSPF
from GraphExt import GraphExt
from InsertionSorter import InsertionSorter, InsertionSorterItem

class ShortestPathIS:
    """
    This class realizes rare-event importance sampling
    for the problem of finding a path length excess probability,
    
        P(S(X) >= \gamma) 
        
    where S(X) is the length of the shortest path
    between two dedicated nodes S and T of a given
    fixed undirected weighted graph G, and X = \vec X
    denotes the random edge weights.
    
    The setting, the specific example graph, and the 
    IS algorithm implemented are from [1].
    
    The implementation uses a graph class having directed
    edges; thus the example inserts a pair of edges for
    each edge in the original task spec of [1].
    
    [1] Reuven Y. Rubinstein, Dirk P. Kroese: "The Cross-Entropy
        method". Springer, 2004.
    """
    
    def __init__(self, G, start_node, end_node):
        self.G = G
        self.S = start_node
        self.T = end_node
        
    def get_shortest_path(self, X): 
        """
        X is expected to be a vector of edge weights, ordered in the
        order as edges were originally added to G. If shorter,
        the vector's values are repeated.
        
        Returns (length, path), where path is a list of vertices. 
        """

        ##print("X:", X)
        for ind in range(self.G.get_number_of_edges()):
            ##print("::", ind)
            edge = self.G.get_edge(ind)
            self.G.set_edge_weight(edge[0], edge[1], X[ind % len(X)])

        d = DijkstraSPF(self.G, self.S)
        ##print(d.get_path(self.T))
        return d.get_distance(self.T), d.get_path(self.T)
        
    def make_exponentially_distr_rv_vec(self, recipr_expectations):
        """
        With m = len(recipr_expectations), returns a vector of length m
        containing in each component i a realization of an exponentially
        distr rv with mean recipr_expectations[i].
        """
        m = len(recipr_expectations)
        res = np.zeros((m,))
        for j in range(m):
            res[j] = rn.expovariate(recipr_expectations[j])
        return res

    def crude_monte_carlo_estimate(self, vec_u, gamma, N):
        """Returns estimate for P(S(X) >= gamma), where the weights
            X are exponentially distributed with (mean) parameters
            specified in vec_u."""
            
        recipr_expectations = np.array([1.0/mu for mu in vec_u])
        sum1 = 0
        for cyc in range(N):
            X = self.make_exponentially_distr_rv_vec(recipr_expectations)
            length_of_sp,_ = self.get_shortest_path(X)
            ##print(length_of_sp)
            if length_of_sp >= gamma: sum1 += 1
        return sum1/N
        
    def imp_sampling_estimate(self, vec_u, vec_v, gamma, N):
        """
        Given the problem description in vec_u and gamma (and, implicitly, G),
        obtains an importance sampling estimate using N data points
        and the importance distribution described by vec_v.
        """
        
        # For the product density 
        #   f(x_1, .., x_m) = \exp( - \sum_j x_j/u_j ) * \prod_j 1/u_j,
        # (j=1..m), the ratio f/g (with vec_v describing the importance 
        # distribution) consequently is
        #   f/g = \exp( - \sum_j x_j (1/u_j - 1/v_j) ) * \prod_j v_j/u_j.
        # Do sample according to g.

        recipr_expectations_u = np.array([1.0/mu for mu in vec_u])
        recipr_expectations_v = np.array([1.0/mu for mu in vec_v])
        difference_recipr_expec = recipr_expectations_u - recipr_expectations_v
        prod1 = np.prod(vec_v)/np.prod(vec_u)
        sum1 = 0
        for cyc in range(N):
            X = self.make_exponentially_distr_rv_vec(recipr_expectations_v)
            length_of_sp,_ = self.get_shortest_path(X)
            ##print(length_of_sp)
            if length_of_sp >= gamma:
                integrand = np.exp(- np.dot(X, difference_recipr_expec)) * prod1
            else:
                integrand = 0
            sum1 += integrand
        return sum1/N

    def imp_sampling_iteration_variant_II(self, vec_u, gamma, N, N_0, \
            n_max_iter, do_debug=False):
        """
        Estimates P(S(X) >= gamma) using IS, adjusting the importance
        distribution in each of the n_max_iter iterations.
        """
        
        m = len(vec_u)
        N_rho = N_0 // 5
        
        # 1. First the discovery runs:
        vec_v = 0.0 + vec_u
        edge_counter = np.zeros((m,))
        for iter in range(n_max_iter):
            if do_debug: print("====================================== ", iter)
            recipr_expectations_u = np.array([1.0/mu for mu in vec_u])
            recipr_expectations_v = np.array([1.0/mu for mu in vec_v])
            difference_recipr_expec = recipr_expectations_u - recipr_expectations_v
            prod1 = np.prod(vec_v)/np.prod(vec_u)
            ins1 = InsertionSorter(max_size=N_rho)
            for cyc in range(N_0):
                X = self.make_exponentially_distr_rv_vec(recipr_expectations_v)
                length_of_sp_v,path = self.get_shortest_path(X)
                ins1.insert(InsertionSorterItem(-length_of_sp_v, path))
            # This has kept us the lengths and paths of the 
            # N_rho longest sh.-path realizations
            if do_debug: print("Upper end sp length:  ", -(ins1.ls[0].k))
            if do_debug: print("Lower end sp length:  ", -(ins1.ls[-1].k))
            if -(ins1.ls[-1].k) > gamma:
                # I.e. all of the N_rho best S(X) samples are above the gamma threshold
                if do_debug: print("I: break condition reached in iteration", iter)
                break
            for inssorter_item in ins1.ls:
                path = inssorter_item.data
                edge_list = self.G.node_path_to_edge_list(path)
                for ind_e in edge_list:
                    edge_counter[ind_e % m] += 1
            vec_v = edge_counter / N_rho
            if do_debug: print("I: New vec_v: ", vec_v)

        # 2. Perform the actual estimate run:
        if do_debug: print("I: Using vec_v: ", vec_v)
        return self.imp_sampling_estimate(vec_u, vec_v, gamma, N)
   
        # The above realizing:
        # Algorithm II (heuristic):
        # 1. With N_0 given, perform N_0 runs, order the resulting S(X)
        #    values and keep the upper fraction of \rho * N_0 pieces.
        # 2. If the lowest S(X) of these is > gamma, we have at least a 
        #    share \rho of samples with large S(X) realization, so stop 
        #    vec_v search iteration and go to 5.
        # 3. Otherwise, determine new weights, v_j', for each edge
        #    by counting the share of runs of the rho N_0 for which
        #    the edge is in a shortest path. (Typically less-weighted
        #    edges will appear more often, thus get overweight in the
        #    next v_j vector, thus are more likely to produce the 
        #    outlier realizations.)
        # 4. Loop to 1
        # 5. Perform the actual estimate run, using N data points.        
            
if __name__ == "__main__":
    S,A,B,T = nodes = list("SABT")
    
    G = GraphExt()
    G.add_edge(S, A, 1)
    G.add_edge(S, B, 1)
    G.add_edge(A, B, 1)
    G.add_edge(A, T, 1)
    G.add_edge(B, T, 1)
    
    # add reverse edges in same order:
    nEdges = G.get_number_of_edges()
    for ind in range(nEdges):
        edge = G.get_edge(ind)
        G.add_edge(edge[1], edge[0], 1)

    spis1 = ShortestPathIS(G, S, T)
    vec_u = 0.1085 * np.array([1.0, 1.0, 1.0, 1.0, 1.0])
    gamma = 0.8
    N = 40000
    
    P_est_CMC = spis1.crude_monte_carlo_estimate(vec_u, gamma, N)
    print("P_est_CMC:   ", P_est_CMC)
