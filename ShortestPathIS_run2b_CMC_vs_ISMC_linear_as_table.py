# (C) 2021 Thomas Pitschel

import numpy as np

from GraphExt import GraphExt
from ShortestPathIS import ShortestPathIS

if __name__ == "__main__":
    S,A,B,T = nodes = list("SABT")
    
    G = GraphExt()
    G.add_edge(S, A, 1)
    #G.add_edge(S, B, 1)
    #G.add_edge(A, B, 1)
    G.add_edge(A, T, 1)
    #G.add_edge(B, T, 1)
    
    # S -> A -> T
    
    # add reverse edges in same order:
    nEdges = G.get_number_of_edges()
    for ind in range(nEdges):
        edge = G.get_edge(ind)
        G.add_edge(edge[1], edge[0], 1)

    print("CMC estimate for linear graph")
    
    #rng = default_rng()
    #rn.set_state(10)
    
    spis1 = ShortestPathIS(G, S, T)
    vec_u = np.array([1.0, 1.0])
    gamma = 15.7
    N = 20000
    
    nRuns = 14
    res = np.zeros((nRuns,6))
    for k in range(nRuns):
        print("----------------------------------")
        gamma = 1.7 + k
        res[k,0] = gamma
        
        res[k,1] = (1+gamma)*np.exp(-gamma)     # = P(X_1+X_2 >= gamma)
        print("P_exact:         ", res[k,1])

        # CMC:
        res[k,2] = spis1.crude_monte_carlo_estimate(vec_u, gamma, N)
        print("P_est_CMC:       ", res[k,2])

        # ISMC:
        N_0 = 2000
        n_max_iter = 10
    
        res[k,4] = spis1.imp_sampling_iteration_variant_II(vec_u, gamma, N, N_0, n_max_iter)
        print("P_est_ISMC_2:    ", res[k,4])
        
        
        # relative errors:
        res[k,3] = np.abs(res[k,2]-res[k,1])/res[k,1]
        res[k,5] = np.abs(res[k,4]-res[k,1])/res[k,1]


    import matplotlib.pyplot as plt
    
    plt.plot(res[:,0], res[:,3], label='crude MC')
    plt.plot(res[:,0], res[:,5], label='importance sampler MC')
    plt.ylim(top=1.05, bottom=-0.05)
    plt.xlabel("gamma")
    plt.ylabel("absolute value of rel. error")
    plt.title("Estimation errors: crude MC vs importance sampling (linear graph)")
    plt.legend()
    plt.show()
