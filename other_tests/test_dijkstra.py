#
# Tests the Dijkstra shortest-path implementation from
# https://github.com/ahojukka5/Dijkstra/blob/master/dijkstra/graph.py
# using the same example as there, but using an extended graph class.
#

from dijkstra import DijkstraSPF
from GraphExt import GraphExt

S, T, A, B, C, D, E, F, G = nodes = list("STABCDEFG")

print(S)

graph = GraphExt()
graph.add_edge(S, A, 4)
graph.add_edge(S, B, 3)
graph.add_edge(S, D, 7)
graph.add_edge(A, C, 1)
graph.add_edge(B, S, 3)
graph.add_edge(B, D, 4)
graph.add_edge(C, E, 1)
graph.add_edge(C, D, 3)
graph.add_edge(D, E, 1)
graph.add_edge(D, T, 3)
graph.add_edge(D, F, 5)
graph.add_edge(E, G, 2)
graph.add_edge(G, E, 2)
graph.add_edge(G, T, 3)
graph.add_edge(T, F, 5)

d = DijkstraSPF(graph, S)

print("%-5s %-5s" % ("label", "distance"))
for u in nodes:
    print("%-5s %8d" % (u, d.get_distance(u)))
    

print(d.get_path(T))
print(" -> ".join(d.get_path(T)))

print("Path as edge list: ", graph.node_path_to_edge_list(d.get_path(T)))
